package net.luency.clock;

public interface Displayable {

  void display();

}
