package net.luency.clock.analogue;

import java.awt.Color;

import java.util.Objects;
import java.util.StringJoiner;
import java.util.concurrent.TimeUnit;

public final class ClockHandSpecifications {
  private static final float FULL_CIRCLE_ANGLE = 360.0F;
  private final Color color;
  private final TimeUnit unit;
  private final double width;
  private final double height;

  private ClockHandSpecifications(Color color, TimeUnit unit, double width, double height) {
    this.color = color;
    this.unit = unit;
    this.width = width;
    this.height = height;
  }

  public Color getColor() {
    return color;
  }

  public TimeUnit getUnit() {
    return unit;
  }

  public double getWidth() {
    return width;
  }

  public double getHeight() {
    return height;
  }

  public double getTurnAngle() {
    if (unit == TimeUnit.SECONDS) {
      return FULL_CIRCLE_ANGLE / TimeUnit.MINUTES.toSeconds(1);
    } else if (unit == TimeUnit.MINUTES) {
      return FULL_CIRCLE_ANGLE / TimeUnit.HOURS.toMinutes(1);
    } else if (unit == TimeUnit.HOURS) {
      // we use / 2.0F as we only have half a day in hours
      return FULL_CIRCLE_ANGLE / (TimeUnit.DAYS.toHours(1) / 2.0F);
    } else if (unit == TimeUnit.MILLISECONDS) {
      return FULL_CIRCLE_ANGLE / (TimeUnit.SECONDS.toMillis(1));
    }
    throw new IllegalStateException(String.format("Illegal timeunit %s has been set!", unit));
  }

  @Override
  public String toString() {
    return new StringJoiner(", ",
      ClockHandSpecifications.class.getSimpleName() + "[", "]")
      .add("color=" + color)
      .add("unit=" + unit)
      .add("width=" + width)
      .add("height=" + height)
      .toString();
  }

  @Override
  public boolean equals(Object other) {
    if (this == other) {
      return true;
    }
    if (other == null || getClass() != other.getClass()) {
      return false;
    }
    ClockHandSpecifications that = (ClockHandSpecifications) other;
    return Double.compare(that.width, width) == 0 &&
      Double.compare(that.height, height) == 0 &&
      Objects.equals(color, that.color) &&
      unit == that.unit;
  }

  @Override
  public int hashCode() {
    return Objects.hash(color, unit, width, height);
  }

  static ClockHandSpecifications create(Color color, TimeUnit unit, double width, double height) {
    Objects.requireNonNull(color, "color");
    Objects.requireNonNull(unit, "unit");
    return new ClockHandSpecifications(color, unit, width, height);
  }
}