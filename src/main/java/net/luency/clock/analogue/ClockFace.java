package net.luency.clock.analogue;

import java.awt.Color;
import java.util.StringJoiner;

import sas.Circle;
import sas.Text;

public final class ClockFace {
  static final int BORDER_DISTANCE = 100;
  private final Circle clockFace;

  private ClockFace() {
    clockFace = new Circle(BORDER_DISTANCE, BORDER_DISTANCE, 500, Color.GRAY);
  }

  void displayHours() {
    Text text = new Text(575, 110, "12", Color.BLACK);
    text.setFontSerif(true, 60);
    text = new Text(587.5, 1010, "6", Color.BLACK);
    text.setFontSerif(true, 60);
    text = new Text(140, 565, "9", Color.BLACK);
    text.setFontSerif(true, 60);
    text = new Text(1025, 565, "3", Color.BLACK);
    text.setFontSerif(true, 60);
  }

  Circle getClockFace() {
    return clockFace;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", ClockFace.class.getSimpleName() + "[", "]")
      .add("clockFace=" + clockFace)
      .toString();
  }

  static ClockFace create() {
    return new ClockFace();
  }
}