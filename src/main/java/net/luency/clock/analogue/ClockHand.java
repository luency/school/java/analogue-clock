package net.luency.clock.analogue;

import java.time.LocalDateTime;
import java.util.Objects;

import java.util.StringJoiner;
import java.util.concurrent.TimeUnit;
import sas.Shapes;
import sas.Rectangle;

import net.luency.clock.Displayable;

public final class ClockHand implements Displayable {
  private final ClockHandSpecifications specifications;
  private final Shapes center;
  private final AnalogueClock clock;
  private int lastUnitAmount;

  private Rectangle hand;

  private ClockHand(ClockHandSpecifications specifications, AnalogueClock clock) {
    this.specifications = specifications;
    this.clock = clock;
    this.center = clock.getClockFace();
  }

  private Rectangle createFromSpecsAndCenter() {
    return new Rectangle(
      center.getCenterX(),
      center.getCenterY(),
      specifications.getWidth(),
      specifications.getHeight(),
      specifications.getColor()
    );
  }

  static ClockHand create(ClockHandSpecifications specifications, AnalogueClock clock) {
    Objects.requireNonNull(specifications, "specifications");
    Objects.requireNonNull(clock, "clock");
    return new ClockHand(specifications, clock);
  }

  void initialize() {
    this.hand = createFromSpecsAndCenter();
  }

  @Override
  public void display() {
    if (hand == null) {
      initialize();
    }
    orientate();
    lastUnitAmount = getUnitAmount();
  }

  private void orientate() {
    hand.turn(center.getCenterX(), center.getCenterY(), 180);
    int amount = getUnitAmount();
    double angle = specifications.getTurnAngle() * amount;
    hand.turn(center.getCenterX(), center.getCenterY(), angle);
  }

  private int getUnitAmount() {
    TimeUnit unit = specifications.getUnit();
    LocalDateTime time = clock.getCurrentTime();
    if (unit == TimeUnit.SECONDS) {
      return time.getSecond();
    } else if (unit == TimeUnit.MINUTES) {
      return time.getMinute();
    } else if (unit == TimeUnit.HOURS) {
      return time.getHour();
    } else if (unit == TimeUnit.MILLISECONDS) {
      return (int) TimeUnit.NANOSECONDS.toMillis(time.getNano());
    }
    throw new IllegalStateException(String.format("Illegal timeunit %s has been set!", unit));
  }

  public void update() {
    if (lastUnitAmount == getUnitAmount()) {
      return;
    }
    lastUnitAmount = getUnitAmount();
    double angle = specifications.getTurnAngle();
    hand.turn(center.getCenterX(), center.getCenterY(), angle);
  }

  Rectangle getHand() {
    return hand;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", ClockHand.class.getSimpleName() + "[", "]")
      .add("specifications=" + specifications)
      .add("center=" + center)
      .add("clock=" + clock)
      .add("lastUnitAmount=" + lastUnitAmount)
      .add("hand=" + hand)
      .toString();
  }
}