package net.luency.clock.analogue;

import java.awt.Color;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import sas.Shapes;
import sas.View;

import net.luency.clock.Clock;

public final class AnalogueClock implements Clock {
  private final ScheduledExecutorService updateScheduler;
  private final ClockFace face;
  private final Shapes center;

  private AnalogueClock() {
    this.updateScheduler = Executors.newScheduledThreadPool(Runtime.getRuntime().availableProcessors() * 2);
    this.face = ClockFace.create();
    this.center = face.getClockFace();
  }

  @Override
  public void display() {
    View view = new View(
      (int) ((int) center.getShapeWidth() + (center.getShapeX() * 2)),
      (int) ((int) center.getShapeHeight() + (center.getShapeY() * 2))
    );
    view.setBackgroundColor(Color.DARK_GRAY);
    face.displayHours();
    displayHours();
    displayMinutes();
    displaySeconds();
  }

  private void displayHours() {
    ClockHandSpecifications specifications = createSpecifications(
      Color.BLACK,
      TimeUnit.HOURS,
      10,
      (center.getShapeHeight() / 2) - 250
    );
    ClockHand hand = ClockHand.create(specifications, this);
    hand.display();
    updateScheduler.scheduleAtFixedRate(hand::update, 1, 1, TimeUnit.SECONDS);
  }

  private void displayMinutes() {
    ClockHandSpecifications specifications = createSpecifications(
      Color.BLACK,
      TimeUnit.MINUTES,
      10,
      (center.getShapeHeight() / 2) - 50
    );
    ClockHand hand = ClockHand.create(specifications, this);
    hand.display();
    updateScheduler.scheduleAtFixedRate(hand::update, 1, 1, TimeUnit.SECONDS);
  }

  private void displaySeconds() {
    ClockHandSpecifications specifications = createSpecifications(
      Color.RED,
      TimeUnit.SECONDS,
      4,
      (center.getShapeHeight() / 2) - 20
    );
    ClockHand hand = ClockHand.create(specifications, this);
    hand.display();
    updateScheduler.scheduleAtFixedRate(hand::update, 1, 1, TimeUnit.SECONDS);
  }

  private ClockHandSpecifications createSpecifications(Color color, TimeUnit unit, double width, double height) {
    return ClockHandSpecifications.create(color, unit, width, height);
  }

  @Override
  public LocalDateTime getCurrentTime() {
    return LocalDateTime.now();
  }

  public static AnalogueClock create() {
    return new AnalogueClock();
  }

  public Shapes getClockFace() {
    return center;
  }
}