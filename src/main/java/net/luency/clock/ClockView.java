package net.luency.clock;

import net.luency.clock.analogue.AnalogueClock;

public final class ClockView {

  private ClockView() {}

  public static void main(String[] args) {
    AnalogueClock.create().display();
  }
}