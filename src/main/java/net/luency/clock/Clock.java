package net.luency.clock;

import java.time.LocalDateTime;

public interface Clock extends Displayable {

  LocalDateTime getCurrentTime();

}
