package net.luency.clock.analogue;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

final class ClockFaceTest {

  @Test
  public void create() {
    ClockFace first = ClockFace.create();
    ClockFace second = ClockFace.create();
    assertSame(first.getClockFace().getColor(), second.getClockFace().getColor(), "Color isn't equal!");
  }
}