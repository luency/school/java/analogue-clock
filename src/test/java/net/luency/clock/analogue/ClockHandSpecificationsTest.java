package net.luency.clock.analogue;

import java.awt.Color;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

final class ClockHandSpecificationsTest {

  @Test
  public void create() {
    Color color = Color.BLACK;
    TimeUnit unit = TimeUnit.SECONDS;
    double width = -1.0;
    double height = -2.0;
    ClockHandSpecifications specs = create(color, unit, width, height);
    assertSame(specs.getColor(), color, "Color isn't equal!");
    assertSame(specs.getUnit(), unit, "TimeUnit isn't equal!");
    assertEquals(specs.getWidth(), width, "width isn't equal!");
    assertEquals(specs.getHeight(), height, "height isn't equal!");
    assertEquals(specs, create(color, unit, width, height), "Specifications aren't equal");
  }

  private ClockHandSpecifications create(
    Color color,
    TimeUnit unit,
    double width,
    double height
  ) {
    return ClockHandSpecifications.create(
      color,
      unit,
      width,
      height
    );
  }
}