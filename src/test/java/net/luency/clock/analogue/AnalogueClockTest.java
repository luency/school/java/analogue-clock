package net.luency.clock.analogue;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

final class AnalogueClockTest {
  private AnalogueClock clock;

  @BeforeEach
  public void prepare() {
    clock = AnalogueClock.create();
  }

  @Test
  public void display() {
    clock.display();
  }

  @Test
  public void testTime() {
    LocalDateTime now = LocalDateTime.now();
    LocalDateTime clockTime = clock.getCurrentTime();
    assertEquals(now.getHour(), clockTime.getHour(), "Time isn't equal");
    assertEquals(now.getMinute(), clockTime.getMinute(), "Time isn't equal");
    assertEquals(now.getDayOfWeek(), clockTime.getDayOfWeek(), "Time isn't equal");
  }
}