package net.luency.clock.analogue;

import java.awt.Color;
import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

final class ClockHandTest {

  @Test
  public void create() {
    double width = 100.0D;
    double height = 150.0D;
    ClockHand hand = ClockHand.create(
      ClockHandSpecifications.create(Color.RED, TimeUnit.SECONDS, width, height),
      AnalogueClock.create()
    );
    hand.initialize();
    assertEquals(width, hand.getHand().getShapeWidth(), "Width isn't equal!");
    assertEquals(height, hand.getHand().getShapeHeight(), "Height isn't equal!");
  }
}